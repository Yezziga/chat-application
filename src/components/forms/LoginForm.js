import React from 'react';
import {  Redirect } from 'react-router-dom';
import axios from 'axios';
import { Button, Input } from 'reactstrap';

class LoginForm extends React.Component {

    constructor(props) {      
        super(props) 
        this.onLoginClicked = this.onLoginClicked.bind(this)
        this.onUsernameChanged = this.onUsernameChanged.bind(this)
        this.state = {
            username: '',
            // apiUrl: 'http://localhost:8080',
            apiUrl: 'https://chat-app-jq.herokuapp.com',
            status: '',
            toDashboard: false
        }
    }

    async onLoginClicked(ev) {
        console.log(this.state.username);
        console.log("btn clicked");
        if(this.state.username) {     
            
            localStorage.setItem('username', this.state.username);
            let token = await axios.get(this.state.apiUrl + `/jwt`)
            .then(res=> {return res.data})
            // console.log(token);
            localStorage.setItem('token', token)
            this.setState({toDashboard: true})
        } else {
            this.setState({status: 'Invalid username.'})
        }
    }    

    onUsernameChanged(ev) {
        this.setState({username: ev.target.value});
        this.setState({status: ''})
    }

    render() {
    
        axios.interceptors.request.use(
            config => {
                const { origin } = new URL(config.url);
                const allowedOrigins = [this.apiUrl]
                const token = localStorage.getItem('token');
                if (allowedOrigins.includes(origin)) {
                    config.headers.authorization = `Bearer ${token}`;
                }
                return config;
             },
                error => {
                  return Promise.reject(error);
            }
        )

        if(this.state.toDashboard === true) {
        
            return <Redirect to='/dashboard' />
        }

        

        return (
            <div className="username-div">
                <label className="bla">Username: </label> {' '}
                <Input size="sm" className="w-25" type="text" onChange = {this.onUsernameChanged} /> {' '}
                
                {' '} <Button color="primary" type="button" onClick = {this.onLoginClicked}>Login</Button>
                <p> {this.state.status} </p>
            </div>
        )
    };
}

export default LoginForm