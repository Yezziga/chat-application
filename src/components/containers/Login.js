import React from 'react';
import { Redirect } from 'react-router-dom';
import LoginForm from '../forms/LoginForm';

// TODO: use jwt authentication
// Redirect to /dashboard if already logged in

class Login extends React.Component {

    render() {
        if (localStorage.getItem('username') && localStorage.getItem('token')) {
            return <Redirect to='/dashboard' />
        }
        return (
            <div>
                <h1>Login</h1>
                <br />
                <LoginForm />
            </div>
        )

    }

};

export default Login;