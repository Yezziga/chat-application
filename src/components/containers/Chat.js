import React from 'react';
import io from 'socket.io-client'
import axios from 'axios';
import { Redirect } from 'react-router-dom'
import ScrollToBottom from 'react-scroll-to-bottom'
import { Row, Input, Button, Col, InputGroup, InputGroupAddon, InputGroupText } from 'reactstrap';


class Chat extends React.Component {

    constructor(props) {
        super(props)
        this.onInputChanged = this.onInputChanged.bind(this)
        this.onSendClicked = this.onSendClicked.bind(this)
        this.onColourPicked = this.onColourPicked.bind(this)

        this.state = {
            username: localStorage.getItem('username'),
            isAuthorized: false,
            message: '',
            // colour: '',
            // apiUrl: 'http://localhost:8080',
            // socket: io('http://localhost:8080',
                apiUrl: 'https://chat-app-jq.herokuapp.com',
                socket: io('https://chat-app-jq.herokuapp.com/',
                {
                    transportOptions:
                    {
                        polling:
                        {
                            extraHeaders:
                            {
                                Authorization: localStorage.getItem('token')
                            }
                        }
                    }
                }),
            messages: [],
            users: []
        }




        this.state.socket.on('connect', () => {
            //socket.emit('setUser', "TEST");
            this.state.socket.emit('username', this.state.username)

            this.state.socket.on('message', data => {

                let msgText = data.user + ": " + data.message
                if (data.user === 'SERVER' && data.message === 'unauthorized') {
                    this.setState({ isAuthorized: false })
                }
                // console.log(data);
                this.setState({ messages: [...this.state.messages, msgText] })
            });

            this.state.socket.on('addUser', data => {
                // console.log(data + ' addUser')
                // this.setState( users=> {
                //     users.list.concat(data)
                // })

                this.setState({ users: [...this.state.users, data] })
            });

            this.state.socket.on('addUsers', data => {
                // console.log(data );
                this.setState({ users: data })
            })

            this.state.socket.on('removeUser', data => {
                // console.log(data + ' to remove');
                this.setState({ users: this.state.users.filter(user => user !== data) })
            });

        });

    }

    async verifyToken() {
        console.log('in verify');
        await axios.get(this.state.apiUrl + `/verify`)
            .then(res => {
                console.log(res);

                return res.data;
            }).catch(err => {
                console.log(err);

            })
    }

    onInputChanged(ev) {
        this.setState({ message: ev.target.value })
    }

    onSendClicked(ev) {
        if (this.state.message) {
            // this.verifyToken()
            this.state.socket.emit('verify', localStorage.getItem('token'))
            this.state.socket.emit('message', { user: this.state.username, message: this.state.message })
        }
    }

    onColourPicked(ev) {
        this.setState({ colour: ev.target.value })
        // this.state.socket.emit('color', ev.target.value)
    }

    render() {
        let memberList = this.state.users.map((member, i) =>
            <p key={"user-" + i}> {member} </p>
        );

        let messages = this.state.messages.map((message, i) =>
            <p key={"message-" + i} style={{ color: this.state.colour }} > {message} </p>
        )

        // if(!this.state.isAuthorized) {
        //     return <Redirect to='/login' />
        // }

        return (
            <div>
                <h1>Welcome to the chat, {this.state.username} </h1>

                <Row>
                    <Col>
                        <p><b>Online users</b></p>
                        {memberList}
                    </Col>
                    <Col>
                        <ScrollToBottom className="scroller" >
                            {messages}
                        </ScrollToBottom>
                    </Col>
                    <Col />
                </Row>
                <br />
                <Row>
                    <Col />
                    <Col>

                        <Input type="textarea" onChange={this.onInputChanged} rows="4" cols="50" style={{ resize: "none" }} />

                        <Button color="primary" onClick={this.onSendClicked} >Send</Button> <br /><br />
                        <InputGroup>
                            <Input onChange={this.onColourPicked} type="color" id="colour" value="#000000" style={{ padding: "5px" }} />

                            <InputGroupAddon addonType="append">
                                <InputGroupText>Colour</InputGroupText>
                            </InputGroupAddon>
                        </InputGroup>
                    </Col>
                    <Col />

                </Row>
            </div>
        )
    }
};

export default Chat;