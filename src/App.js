import React from 'react';
import {BrowserRouter as Router, Switch, Route, Link, Redirect} from 'react-router-dom';
import './App.css';
import Login from './components/containers/Login';
import Chat from './components/containers/Chat';

class App extends React.Component {
  constructor() {
    super()
    this.state = { 
      username: localStorage.getItem('username'),
      loggedIn: false
    }
  }
  
  render() {

    return (
      <Router>
        <div className="App">
          <header className= "App-header">
            <p> {this.state.username} </p>  
            <Link className="App-link" to="/login"> Login</Link>
            <Link className="App-link" to="/dashboard">Chat</Link>
          </header>

          <Switch>
            <Route path="/login" component= {Login} />
            <Route path="/dashboard"  component= {Chat} />  
          </Switch>
        </div>
      </Router>
    );
}
}

export default App;
